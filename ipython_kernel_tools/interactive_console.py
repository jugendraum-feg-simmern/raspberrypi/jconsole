from ipykernel.kernelapp import IPKernelApp
try:
    from ipykernel.connect import connect_qtconsole
except ImportError:
    try:
        # deprecated since IPython 4.0
        from IPython.kernel.connect import connect_qtconsole
    except ImportError:
        # deprecated since IPython 1.0
        from IPython.lib.kernel import connect_qtconsole


class InteractiveConsole:
    def __init__(self, gui):
        self.kernel = IPKernelApp.instance()
        self.kernel.initialize(['python', '--matplotlib={}'.format(gui)])
        self.namespace = self.kernel.shell.user_ns
        self._console = None

    def show(self):
        """
        Since connect_qtconsole() uses system.Popen() to basically run `ipython qtconsole`
        and attach it to a previously created kernel, this creates the window.
        """
        self._console = connect_qtconsole(self.kernel.abs_connection_file,
                                          profile=self.kernel.profile,
                                          argv=['--style', 'vim', '--no-confirm-exit'])

    def add_object(self, name, obj):
        name = str(name)
        self.namespace.update({name: obj})

