import argparse
from PyQt5.QtWidgets import QApplication

from jClient import jClient
from ipython_kernel_tools.interactive_console import InteractiveConsole


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('host', type=str, help='hostname of device running jServer')
    parser.add_argument('-p', '--port', default=6330, type=int, help='port of jServer')
    args = parser.parse_args()

    # start the qt event loop
    app = QApplication([])
    # create an interactive console
    console = InteractiveConsole('qt')

    # create a jClient object
    jcl = jClient(app, console, args.host, args.port)

    # add jcl to the namespace of console and star
    console.add_object('jcl', jcl)
    console.show()
    console.kernel.start()
