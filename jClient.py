from PyQt5.QtCore import QObject, QTimer, QElapsedTimer
from APIParameter import APIParameter
from asio.communication import Communication

performance_timer = QElapsedTimer()

errno = {
    0: "OK",
    1: "ECONVERSION",
    2: "ENOTAVAIL",
    3: "ENOTIMPL",
    4: "EOUTOFRANGE",
    74: "EBADMSG",
    95: "ENOTSUP",
    110: "ETIMEDOUT",
    127: "ENULLPTR"
}

class jClient(QObject):
    def __init__(self, app, console, host, port, **kwarg):
        super().__init__(**kwarg)
        self.app = app
        self.console = console

        # setup tcp socket
        self.host, self.port = host, port
        self.comm = Communication(host, port)
        self.comm.connected_signal.connect(self.on_connected)
        self.comm.disconnected_signal.connect(self.on_disconnected)

        # setup qtconsole exit detection
        self._qtconsole_check_timer = QTimer(self)
        self._qtconsole_check_timer.setInterval(100)
        self._qtconsole_check_timer.timeout.connect(self._qtconsole_check)
        self._qtconsole_check_timer.start()

        # dict to hold parameters
        self.api_parameters = dict()
        self._add_api_parameter('APIParameters',
                                custom_accessor=self._request_api_parameters,
                                custom_update_handler=self._load_parameters)

    def _get_api_parameter(self, param, *args):
        msg = [0, param.name]
        if args:
            for i in args:
                msg.append(i)
        promise = self.comm.request(msg)
        # promise.then(param.update_handler)
        promise.wait_for_response()
        if promise.err_num is None or promise.err_num == 0:
            return promise.response
        else:
            return self._create_error_output(promise)

    def _set_api_parameter(self, param, *args):
        msg = [1, param.name]
        if args:
            for i in args:
                msg.append(i)
        promise = self.comm.request(msg)
        # promise.then(param.update_handler)
        promise.wait_for_response()
        if promise.err_num is None or promise.err_num == 0:
            return
        else:
            return self._create_error_output(promise)

    def _add_api_parameter(self, name, custom_accessor=None, custom_mutator=None, custom_update_handler=None):
        acc = custom_accessor if custom_accessor else self._get_api_parameter
        mut = custom_mutator if custom_mutator else self._set_api_parameter
        hdl = custom_update_handler
        p = APIParameter(name, acc, mut, hdl)
        setattr(self, name, p)
        self.api_parameters[name] = p

    def _load_parameters(self, params):
        for p in params:
            p = p.decode()
            if p not in self.api_parameters.keys():
                self._add_api_parameter(p)

    def _request_api_parameters(self, *args):
        msg = [0, 'APIParameters']
        promise = self.comm.request(msg)
        promise.then(self.APIParameters.update_handler)
        promise.wait_for_response(timeout=60000)
        return self.api_parameters

    def measure_response_time_ms(self, func, *args):
        performance_timer.start()
        func(*args)
        return performance_timer.elapsed()

    def _qtconsole_check(self):
        if self.console._console is not None:
            if self.console._console.poll() is not None:
                self.close()

    def _create_error_output(self, promise):
        try:
            err_code = errno[promise.err_num]
        except KeyError:
            err_code = promise.err_num

        out = 'ERROR [{}]'.format(err_code)
        if promise.err_msg:
            out += ': {}'.format(promise.err_msg)

        return out

    def on_connected(self):
        self.APIParameters.get()

    def on_disconnected(self):
        self.api_parameters.clear()

    def close(self):
        self._qtconsole_check_timer.stop()
        self.console.kernel.exit()
        self.app.quit()
