# jConsole

## 1. Installation
Get the repo
```shell
git clone https://gitlab.com/jugendraum-feg-simmern/raspberrypi/jconsole.git
cd jconsole
```
Install required python packages
```shell
pip3 install -r requirements.txt
```
Install some more packages with apt
```shell
sudo apt install jupyter-qtconsole
```
*Note:* On Arch based systems use the `python-qtconsole` package.

## 2. Usage
Just run
```shell
python3 jConsole.py <ip of pi>
```
For more options and further explanation run
```shell
python3 jConsole.py --help
```
or have a look at the source code ;)
