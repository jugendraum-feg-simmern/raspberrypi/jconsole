import msgpack
from .promise import Promise
from PyQt5.QtCore import QObject, QTimer
from PyQt5.QtNetwork import QTcpSocket
from PyQt5.QtCore import pyqtSignal


class Communication(QObject):
    # create some signals
    # need to be defined outside __init__() to be a class variable instead of instance variable
    api_parameter_updated = pyqtSignal(str, list)
    connected_signal = pyqtSignal()
    disconnected_signal = pyqtSignal()

    def __init__(self, host, port, **kwargs):
        super().__init__(**kwargs)

        # setup tcp socket
        self.host, self.port = host, port
        self._tcp_socket = QTcpSocket(self)
        self._tcp_socket.readyRead.connect(self.on_ready_read)
        self._tcp_socket.connected.connect(self.on_connect)
        self._tcp_socket.disconnected.connect(self.on_disconnect)

        # setup automatic connection
        self._auto_connect_timer = QTimer(self)
        self._auto_connect_timer.setInterval(1000)
        self._auto_connect_timer.timeout.connect(self._auto_connect)
        self._auto_connect_timer.start()

        # setup msgpack
        self.unpacker = msgpack.Unpacker(raw=True)

        # keep track of promises made
        self._promises = dict()

    def on_ready_read(self):
        unpacker_food = self._tcp_socket.readAll()
        self.unpacker.feed(unpacker_food)
        for msg in self.unpacker:
            self.handle_message(msg)

    def handle_message(self, msg):
        param = msg[0].decode()  # convert bytes to string
        ret_val = int(msg[1])
        response = msg[2:]

        if param in self._promises.keys():
            promise = self._promises.pop(param)
            if ret_val == 0:
                promise.handle_response(response)
            else:
                promise.handle_error(ret_val, response)
        else:
            print('out of context parameter updated: msg = {}'.format(msg))
            self.api_parameter_updated.emit(param, response)

    def request(self, msg):
        if not isinstance(msg, Promise):
            promise = Promise(msg, self._tcp_socket)
        else:
            promise = msg

        # use name of requested parameter as key
        # TODO: implement promise ID to allow for multiple requests of the same parameter
        self._promises[promise.message[1]] = promise
        self.send_message(promise.message)
        return promise

    def send_message(self, msg):
        packed = msgpack.packb(msg, use_bin_type=True)
        self._tcp_socket.write(packed)
        self._tcp_socket.waitForBytesWritten()

    def on_connect(self):
        print('\nconnected')
        self.connected_signal.emit()

    def on_disconnect(self):
        print('\ndisconnected')
        self._auto_connect_timer.start()
        self.disconnected_signal.emit()

    def _auto_connect(self):
        if self._tcp_socket.state() == self._tcp_socket.ConnectedState:
            self._auto_connect_timer.stop()
        else:
            self._tcp_socket.connectToHost(self.host, self.port)
