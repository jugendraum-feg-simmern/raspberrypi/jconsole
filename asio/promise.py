from PyQt5.QtCore import QObject, QEventLoop, QTimer, pyqtSignal


class Promise(QObject):
    handled_signal = pyqtSignal()

    def __init__(self, msg, socket, **kwargs):
        super().__init__(**kwargs)
        self.message = msg
        self.socket = socket
        self.callback = None
        self.response = None
        self.err_num, self.err_msg = None, None

    def then(self, callback):
        if self.response is not None:
            callback(self.response)
            return

        self.callback = callback

    def handle_response(self, response):
        self.response = response[0] if len(response) == 1 else response
        self.handled_signal.emit()
        if self.callback is None:
            return
        self.callback(response)

    def handle_error(self, err_num, err_msg):
        self.err_num = err_num
        try:
            self.err_msg = err_msg[0].decode()
        except AttributeError:
            self.err_msg = None
        self.handled_signal.emit()
        # print('ERROR [{}]: {}'.format(self.err_num, self.err_msg))

    def wait_for_response(self, timeout=10000):
        if self.response is not None and (self.err_num is not None or self.err_num != 0):
            return

        # Thanks to jdreaver fot the suggestion on how to implement this block_until_emit functionality
        # https://github.com/pytest-dev/pytest-qt/issues/12
        loop = QEventLoop()
        self.handled_signal.connect(loop.quit)
        QTimer.singleShot(timeout, loop.quit)
        loop.exec_()

        if self.response is None and (self.err_num is None or self.err_num == 0):
            raise TimeoutError(self.message)

