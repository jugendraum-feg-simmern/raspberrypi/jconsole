class APIParameter:
    def __init__(self, name, accessor, mutator, custom_update_handler=None):
        self.name = name
        self.accessor = accessor
        self.mutator = mutator
        self.update_handler = custom_update_handler if custom_update_handler else self.default_update_handler

    def set(self, *args):
        return self.mutator(self, *args)

    def get(self, *args):
        return self.accessor(self, *args)

    def default_update_handler(self, val):
        print("{name} changed to {val}".format(name=self.name, val=val))
